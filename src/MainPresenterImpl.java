import java.awt.*;
import java.util.ArrayList;
import java.util.List;

public class MainPresenterImpl implements MainPresenter {
    private MainView view;
    private List<Cell> cellList = new ArrayList<>();

    public MainPresenterImpl(MainView view) {
        this.view = view;
    }

    @Override
    public void showResult(int idString) {
        if (idString == -1) {
            view.showErrorMessage("Выбери число, чепушило!");
        } else {
            initCellList();
            int resultRoulette = Roulette.getNumber();
            Cell cell = cellList.get(resultRoulette);
            String nameColor = null;
            Color color = null;
            if (cell.getColorCell() == ColorCell.Black) {
                nameColor = "черное";
                color = Color.black;
            }
            if (cell.getColorCell() == ColorCell.Red) {
                nameColor = "красное";
                color = Color.red;
            }
            view.showResult(String.format("Выиграло %d ", cell.getNumber()), nameColor);
            view.setColorResult(color);

        }
    }

    private void initCellList() {
        for (int i = 0; i <= 36; i++) {
            if (i == 0) {
                cellList.add(new Cell(i, ColorCell.NotColor));
            }
            if ((i % 2) == 0) {
                cellList.add(new Cell(i, ColorCell.Black));
            } else {
                cellList.add(new Cell(i, ColorCell.Red));
            }
        }
    }
}
