public class Cell {
    private int number;
    private ColorCell colorCell;

    public Cell(int number, ColorCell colorCell) {
        this.number = number;
        this.colorCell = colorCell;
    }

    public int getNumber() {
        return number;
    }

    public ColorCell getColorCell() {
        return colorCell;
    }
}
