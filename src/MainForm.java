import javax.swing.*;
import java.awt.*;

public class MainForm extends JFrame implements MainView {
    private JLabel resultText;
    private JPanel mainPanel;
    private JComboBox listNumbers;
    private JButton showResultBtn;
    private JLabel resultColorText;
    private MainPresenter presenter;

    public MainForm() {
        getContentPane().add(mainPanel);
        showResultBtn.addActionListener(e -> presenter.showResult(listNumbers.getSelectedIndex()));
        presenter = new MainPresenterImpl(this);
    }

    @Override
    public void showResult(String text, String text2) {
        resultText.setText(text);
        resultColorText.setText(text2);
    }

    @Override
    public void showErrorMessage(String text) {
        JOptionPane.showMessageDialog(this, text, "Сообщение программы", JOptionPane.ERROR_MESSAGE);
    }

    @Override
    public void setColorResult(Color color) {
        resultColorText.setForeground(color);
    }
}
