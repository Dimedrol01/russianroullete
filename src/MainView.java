import java.awt.*;

public interface MainView {
    void showResult(String text, String text2);
    void showErrorMessage(String text);
    void setColorResult(Color color);
}
