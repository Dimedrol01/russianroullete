import java.util.Random;

public class Roulette {
    public static int getNumber() {
        final int maxValue = 36;
        Random random = new Random();
        int result = random.nextInt(maxValue);
        if (result > maxValue) {
            result = maxValue;
        }
        return result;
    }
}

