import javax.swing.*;

public class Main {
    public static void main(String... args) {
        JFrame mainForm = new MainForm();
        mainForm.setTitle("Русская рулетка");
        mainForm.setSize(300, 300);
        mainForm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainForm.setResizable(false);
        mainForm.setLocationRelativeTo(null);
        mainForm.setVisible(true);
    }
}
